package com.satori.app.managers
{
	import flash.events.EventDispatcher;
	import flash.events.IEventDispatcher;
	
	public class EventDispatcher extends flash.events.EventDispatcher
	{
		public function EventDispatcher(target:IEventDispatcher=null)
		{
			super(target);
		}
	}
}