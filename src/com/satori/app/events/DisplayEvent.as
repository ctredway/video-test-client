package com.satori.app.events
{
	import flash.events.Event;
	
	public class DisplayEvent extends Event
	{
		public function DisplayEvent(type:String, bubbles:Boolean=false, cancelable:Boolean=false)
		{
			super(type, bubbles, cancelable);
		}
	}
}