package com.satori.app.events
{
	import flash.events.Event;
	
	public class ClientEvent extends Event
	{
		public function ClientEvent(type:String, bubbles:Boolean=false, cancelable:Boolean=false)
		{
			super(type, bubbles, cancelable);
		}
	}
}